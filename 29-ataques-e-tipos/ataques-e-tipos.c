#include <stdio.h>
#include <stdlib.h>

#define PARAR -1


int main()
{
    int tamanho_matriz;
	scanf(" %i", &tamanho_matriz);

	float matriz_tipos[tamanho_matriz][tamanho_matriz];

	for (int i = 0; i < tamanho_matriz; i++) {
		for (int j = 0; j < tamanho_matriz; j++) {
			scanf(" %f", &matriz_tipos[i][j]);
		}
	}

	int **ataques = NULL;
	int contador_ataques = 0;
	int indice_linha;
	
	do {
		indice_linha = contador_ataques;
		contador_ataques++;
		ataques = realloc(ataques, contador_ataques * sizeof(int *));
		ataques[indice_linha] = malloc(2 * sizeof(int));
		
		for (int i = 0; i < 2; i++) {
			scanf(" %i", &ataques[indice_linha][i]);

			if (i == 0 && ataques[indice_linha][i] == PARAR) {
				break;
			}
		}
		
	} while (ataques[indice_linha][0] != PARAR);

	int tipo_inimigo;
	scanf(" %i", &tipo_inimigo);
	
	float max_dano = 0;
	int indice_max_dano = 0;

	
	for (int i = 0; i < indice_linha; i++) {
		int tipo_ataque = ataques[i][1];
		int poder_ataque = ataques[i][0];
	   	float fator_multiplicacao = matriz_tipos[tipo_ataque][tipo_inimigo];

		float dano_especifico = poder_ataque * fator_multiplicacao;
		
		if (dano_especifico > max_dano) {
			max_dano = dano_especifico;
			indice_max_dano = i;
		}
	}

	printf("O melhor ataque possui indice %i e dano %.2f\n", indice_max_dano, max_dano);


	for (int i = 0; i < contador_ataques; i++) {
		free(ataques[i]);
	}

	free(ataques);
	
	return 0;
}

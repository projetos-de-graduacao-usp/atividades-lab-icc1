/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <stdio.h>
#include <stdlib.h>

#define FISICO 'p'
#define ESPECIAL 's'
#define CADASTRAR_POKEMON 1
#define CADASTRAR_ATAQUE 2
#define IMPRIMIR_POKEMON 3
#define IMPRIMIR_ATAQUE 4
#define SAIR 0

typedef struct atributos {
	int hp;
	int ataque;
	int defesa;
	int ataque_especial;
	int defesa_especial;
	int velocidade;
} Atributos;

typedef struct ataque {
	char nome[20];
	int poder_base;
	float acuracia;
	char classe;
} Ataque;

typedef struct pokedex {
	char nome[50];
	char tipo_pri[20];
	char tipo_sec[20];
	Atributos atributos;
	Ataque ataques[4];
} Pokedex;

void cadastrar_pokemon(Pokedex *novo_pokemon);
void cadastrar_ataque(Ataque *novo_ataque);
void imprimir_pokemon(Pokedex *pokemon);
void imprimir_ataque(Ataque *ataque);


int main()
{
	Pokedex *lista_pokemon = NULL;
	int cont_pokemon = 0;
	int indice_pokemon;
	int indice_ataque;

	int comando;

	do {
		scanf(" %d", &comando);
		scanf(" %*[\n]");

		switch (comando) {
			case CADASTRAR_POKEMON:
				indice_pokemon = cont_pokemon;
				cont_pokemon++;
				lista_pokemon = realloc(lista_pokemon, cont_pokemon * sizeof(Pokedex));
				cadastrar_pokemon(&lista_pokemon[indice_pokemon]);
				break;

			case CADASTRAR_ATAQUE:
				scanf(" %d", &indice_pokemon);
				scanf(" %d", &indice_ataque);
				scanf(" %*[\n]");
				cadastrar_ataque(&lista_pokemon[indice_pokemon].ataques[indice_ataque]);
				break;

			case IMPRIMIR_POKEMON:
				scanf(" %d", &indice_pokemon);
				scanf(" %*[\n]");
				imprimir_pokemon(&lista_pokemon[indice_pokemon]);
				break;

			case IMPRIMIR_ATAQUE:
				scanf(" %d", &indice_pokemon);
				scanf(" %d", &indice_ataque);
				scanf(" %*[\n]");
				imprimir_ataque(&lista_pokemon[indice_pokemon].ataques[indice_ataque]);
		}

	} while (comando != SAIR);

	free(lista_pokemon);

	return 0;
}


void cadastrar_pokemon(Pokedex *novo_pokemon)
{
	scanf(" %s", novo_pokemon->nome);
	scanf(" %s", novo_pokemon->tipo_pri);
	scanf(" %s", novo_pokemon->tipo_sec);
	scanf(" %d", &novo_pokemon->atributos.hp);
	scanf(" %d", &novo_pokemon->atributos.ataque);
	scanf(" %d", &novo_pokemon->atributos.defesa);
	scanf(" %d", &novo_pokemon->atributos.ataque_especial);
	scanf(" %d", &novo_pokemon->atributos.defesa_especial);
	scanf(" %d", &novo_pokemon->atributos.velocidade);
	scanf(" %*[\n]");
}


void cadastrar_ataque(Ataque *novo_ataque)
{
	scanf(" %s", novo_ataque->nome);
	scanf(" %d", &novo_ataque->poder_base);
	scanf(" %f", &novo_ataque->acuracia);
	scanf(" %c", &novo_ataque->classe);
	scanf(" %*[\n]");
}


void imprimir_pokemon(Pokedex *pokemon)
{
	printf("Nome do Pokemon: %s\n", pokemon->nome);
	printf("Tipo primario: %s\n", pokemon->tipo_pri);
	printf("Tipo secundario: %s\n", pokemon->tipo_sec);
	printf("Status:\n");
	printf("\tHP: %d\n", pokemon->atributos.hp);
	printf("\tAtaque: %d\n", pokemon->atributos.ataque);
	printf("\tDefesa: %d\n", pokemon->atributos.defesa);
	printf("\tAtaque Especial: %d\n", pokemon->atributos.ataque_especial);
	printf("\tDefesa Especial: %d\n", pokemon->atributos.defesa_especial);
	printf("\tVelocidade: %d\n\n", pokemon->atributos.velocidade);
}


void imprimir_ataque(Ataque *ataque)
{
	printf("Nome do Ataque: %s\n", ataque->nome);
	printf("Poder base: %d\n", ataque->poder_base);
	printf("Acuracia: %f\n", ataque->acuracia);
	printf("Classe: %c\n\n", ataque->classe);
}

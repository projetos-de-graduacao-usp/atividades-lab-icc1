#include <stdio.h>

#define LINHAS 32
#define COLUNAS 64
#define AR ' '
#define AREIA '#'
#define AGUA '~'
#define CIMENTO '@'

void atualizar_fisica(char tela[LINHAS][COLUNAS]);


int main()
{
	char tela[LINHAS][COLUNAS];

	for (int i = 0; i < LINHAS; i++) {
		for (int j = 0; j < COLUNAS; j++) {
			tela[i][j] = ' ';
		}
	}

	int numero_frames;
	scanf(" %d", &numero_frames);

	int frame, x, y;
	char nova_particula;

	int contador;
	while (contador < numero_frames) {

		// `scanf` retorna EOF quando chega ao fim da entrada.
		int n_lido = scanf(" %d: %d %d %c", &frame, &x, &y, &nova_particula);

		// Se não há mais partículas a serem criadas, continue até o final
		// da simulação.
		if (n_lido == EOF) {
			frame = numero_frames;
		}

		// Calcula todos os frames até o próximo frame onde queremos criar
		// uma partícula.
		while (contador < frame) {
			printf("frame: %d\n", contador + 1);
			for (int i = 0; i < LINHAS; i++) {
				for (int j = 0; j < COLUNAS; j++) {
					printf("%c", tela[i][j]);
				}
				printf("\n");
			}

			atualizar_fisica(tela);

			contador++;
		}

		// Se há uma particula a ser criada, crie ela.
		if (n_lido != EOF) {
			tela[y][x] = nova_particula;
		}
	}


	return 0;
}


void atualizar_fisica (char tela[LINHAS][COLUNAS])
{
	// fazendo a copia da matriz
	char copia_tela[LINHAS][COLUNAS];
	for (int i = 0; i < LINHAS; i++) {
		for (int j = 0; j < COLUNAS; j++) {
			copia_tela[i][j] = tela[i][j];
		}
	}

	for (int i = 0; i < LINHAS; i++) {
		for (int j = 0; j < COLUNAS; j++) {
			char tmp = copia_tela[i][j];

			switch (tela[i][j]) {
				case AREIA:
					// caso o valor de i for igual ao ultimo indice não existe nada abaixo
					if (i == LINHAS - 1) {
						break;
					}

					if (tela[i + 1][j] == AGUA || tela[i + 1][j] == AR) {
						copia_tela[i][j] = copia_tela[i + 1][j];
						copia_tela[i + 1][j] = tmp;
					}
					// se há espaço abaixo e j > 0
					// (se estivermos no indice j == 0 não há epaços a esquerda)
					else if ((tela[i + 1][j - 1] == AGUA || tela[i + 1][j - 1] == AR) && j > 0) {
						copia_tela[i][j] = copia_tela[i + 1][j - 1];
						copia_tela[i + 1][j - 1] = tmp;
					}
					// se há espaço abaixo j é menor que o último indice da coluna
					// (se stivermos no último indice da coluna não há espaço a direita)
					else if ((tela[i + 1][j + 1] == AGUA || tela[i + 1][j + 1] == AR) &&
							 j < COLUNAS - 1) {

						copia_tela[i][j] = copia_tela[i + 1][j + 1];
						copia_tela[i + 1][j + 1] = tmp;
					}
					break;

				case AGUA:
					if (tela[i + 1][j] == AR && i < LINHAS - 1) {
						copia_tela[i][j] = copia_tela[i + 1][j];
						copia_tela[i + 1][j] = tmp;
					}
					// se há espaço abaixo e j > 0
					else if (tela[i + 1][j - 1] == AR && i < LINHAS - 1 && j > 0) {
						copia_tela[i][j] = copia_tela[i + 1][j - 1];
						copia_tela[i + 1][j - 1] = tmp;
					}
					// se há esspaço abaixo e j é menor que o último indice da coluna
					else if (tela[i + 1][j + 1] == AR && i < LINHAS - 1 && j < COLUNAS - 1) {
						copia_tela[i][j] = copia_tela[i + 1][j + 1];
						copia_tela[i + 1][j + 1] = tmp;
					}
					else if (tela[i][j - 1] == AR && j > 0) {
						copia_tela[i][j] = copia_tela[i][j - 1];
						copia_tela[i][j - 1] = tmp;
					}
					else if (tela[i][j + 1] == AR && j < COLUNAS - 1) {
						copia_tela[i][j] = copia_tela[i][j + 1];
						copia_tela[i][j + 1] = tmp;
					}
			}
		}
	}

	// taribuindo os valores da copia para a matriz real
	for (int i = 0; i < LINHAS; i++) {
		for (int j = 0; j < COLUNAS; j++) {
			tela[i][j] = copia_tela[i][j];
		}
	}
}

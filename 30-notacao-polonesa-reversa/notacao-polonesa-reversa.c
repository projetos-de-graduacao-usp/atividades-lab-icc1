/*
  Aluno: Miguel Reis de Araújo
  NºUSP: 12752457
*/

#include <stdio.h>
#include <stdlib.h>

#define SOMA '+'
#define SUBTRACAO '-'
#define MULTIPLICACAO '*'
#define DIVISAO '/'

int main()
{
	double *pilha = NULL;
	int tamanho_pilha = 0;
	int topo_pilha;
	char proximo_valor;

	while (1) {
		topo_pilha = tamanho_pilha;
		tamanho_pilha++;

		int stop = scanf(" %c", &proximo_valor) == EOF;

		if (stop) {
			break;
		}

		double valor_direita = 0;
		double valor_esquera = 0;

		if (tamanho_pilha > 2) {
			valor_direita = pilha[topo_pilha - 1];
			valor_esquera = pilha[topo_pilha - 2];
		}

		double resultado;

		switch (proximo_valor) {
			case SOMA:
				tamanho_pilha -= 2;
				resultado = valor_esquera + valor_direita;

				pilha = realloc(pilha, tamanho_pilha * sizeof(double));

				pilha[tamanho_pilha - 1] = resultado;

				break;

			case SUBTRACAO:
				tamanho_pilha -= 2;
				resultado = valor_esquera - valor_direita;

				pilha = realloc(pilha, tamanho_pilha * sizeof(double));

				pilha[tamanho_pilha - 1] = resultado;

				break;

			case MULTIPLICACAO:
				tamanho_pilha -= 2;
				resultado = valor_esquera * valor_direita;

				pilha = realloc(pilha, tamanho_pilha * sizeof(double));

				pilha[tamanho_pilha - 1] = resultado;

				break;

			case DIVISAO:
				tamanho_pilha -= 2;
				resultado = valor_esquera / valor_direita;

				pilha = realloc(pilha, tamanho_pilha * sizeof(double));

				pilha[tamanho_pilha - 1] = resultado;

				break;

			default:
				pilha = realloc(pilha, tamanho_pilha * sizeof(double));
				ungetc(proximo_valor, stdin);
				scanf(" %lf", &pilha[topo_pilha]);
		}
	}

	printf("Resultado: %.6lf\n", pilha[0]);

	free(pilha);

	return 0;
}

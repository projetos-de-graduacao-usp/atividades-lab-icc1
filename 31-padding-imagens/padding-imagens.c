/*
  Aluno: Miguel Reis de Araújo
  NºUSP: 12752457
*/

#include <stdio.h>


int main()
{
	int largura_imagem;
	int altura_imagem;

	scanf(" %i %i", &largura_imagem, &altura_imagem);

	int imagem[altura_imagem][largura_imagem];

	for (int i = 0; i < altura_imagem; i++) {
		for (int j = 0; j < largura_imagem; j++) {
			scanf(" %i", &imagem[i][j]);
		}
	}

	int tam_borda;

	scanf(" %i", &tam_borda);

	int largura_mod = largura_imagem + (tam_borda * 2);
	int altura_mod = altura_imagem + (tam_borda * 2);

	int mod_imagem[altura_mod][largura_mod];

	int cont_linha = 0;

	for (int i = 0; i < altura_mod; i++) {
		int cont_coluna = 0;

		for (int j = 0; j < largura_mod; j++) {
			if ((i >= tam_borda && i < (tam_borda + altura_imagem)) &&
				(j >= tam_borda && j < (tam_borda + largura_imagem))) {

				mod_imagem[i][j] = imagem[cont_linha][cont_coluna];
				cont_coluna++;
			}
			else {
				mod_imagem[i][j] = 0;
			}
		}

		if (i >= tam_borda && i < (tam_borda + altura_imagem)) {
			cont_linha++;
		}
	}

	
	for (int i = 0; i < altura_mod; i++) {
		for (int j = 0; j < largura_mod; j++) {
			printf("%i ", mod_imagem[i][j]);
		}
		printf("\n");
	}

	printf("\n");

	for (int i = 0; i < altura_imagem; i++) {
		for (int j = 0; j < largura_imagem; j++) {
			printf("%i ", imagem[i][j]);
		}
		printf("\n");
	}

	return 0;
}

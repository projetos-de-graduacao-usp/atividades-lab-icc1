#include <stdio.h>

#define NUM_VALORES 10

int maior(int *valores);
int menor(int *valores);
float media(int *valores);
int moda(int *valores);

int main()
{
	int valores[NUM_VALORES];
	for (int i = 0; i < NUM_VALORES; i++) {
		scanf(" %d", &valores[i]);
	}

	printf("%d ", maior(valores));
	printf("%d ", menor(valores));
	printf("%.2f ", media(valores));
	printf("%d", moda(valores));
	
	return 0;
}


int maior(int *valores)
{
	int maior_valor = valores[0];
	for (int i = 0; i < NUM_VALORES; i++) {
		if (valores[i] > maior_valor)
			maior_valor = valores[i];
	}

	return maior_valor;
}


int menor(int *valores)
{
	int menor_valor = valores[0];
	for (int i = 0; i < NUM_VALORES; i++) {
		if (valores[i] < menor_valor)
			menor_valor = valores[i];
	}

	return menor_valor;
}


float media(int *valores)
{
	float media;
	for (int i = 0; i < NUM_VALORES; i++) {
		media += valores[i];
	}

	media /= 10;

	return media;
}


int moda(int *valores)
{
	int moda = 0;
	int cont_moda = 0;
	for (int i = 0; i < NUM_VALORES; i++) {
		int contador = 0;

		if (valores[i] == moda)
			continue;

		for (int j = 0; j < NUM_VALORES; j++) {
			if (valores[i] == valores[j]) {
				contador += 1;
			}
		}

		if (contador > cont_moda) {
			moda = valores[i];
			cont_moda = contador;
		}
	}

	return moda;
}

#include <stdio.h>

#define JOGADOR_X 'x'
#define JOGADOR_O 'o'
#define INDEFINIDO '-'
#define TAMANHO_TABULEIRO 3
#define VITORIA_X 3
#define VITORIA_O -3

int main()
{
	char tabuleiro[TAMANHO_TABULEIRO][TAMANHO_TABULEIRO];
	
	for (int i = 0; i < TAMANHO_TABULEIRO; i++) {
		for (int j = 0; j < TAMANHO_TABULEIRO; j++) {
			scanf(" %c", &tabuleiro[i][j]);
		}
	}

	int coluna_1 = 0;
	int coluna_2 = 0;
	int coluna_3 = 0;
	char vencedor = INDEFINIDO;
	int pode_empatar = 1;

	/* eu estou considerando uma vitória do jogador "x" se, em qualquer linha reta, o resultado
	   final da variável correspondente (linha, coluna_n, diagonal_n) tiver valor 3 positivo,
	   se for 3 negativo a vitória é do "o", se for qualquer outra coisa significa
	   que ninguém completou essa linha reta*/

	for (int i = 0; i < TAMANHO_TABULEIRO; i++) {
		int linha = 0;

		for (int j = 0; j < TAMANHO_TABULEIRO; j++) {
			switch (tabuleiro[i][j]) {
				case JOGADOR_X:
					linha += 1;

					if (j == 0)
						coluna_1 += 1;

					else if (j == 1)
						coluna_2 += 1;

					else if (j == 2)
						coluna_3 += 1;


					if (j == TAMANHO_TABULEIRO - 1 && linha == VITORIA_X) {
						vencedor = JOGADOR_X;
					}

					break;

				case JOGADOR_O:
					linha -= 1;

					if (j == 0)
						coluna_1 -= 1;

					else if (j == 1)
						coluna_2 -= 1;

					else if (j == 2)
						coluna_3 -= 1;


					if (j == TAMANHO_TABULEIRO - 1 && linha == VITORIA_O) {
						vencedor = JOGADOR_O;
					}

					break;

				case INDEFINIDO:
					pode_empatar = 0;
					// se houver uma casa vazia o jogo ainda não acabou
			}
		}
	}


	if (vencedor == INDEFINIDO) {
		/* se ninguém tiver vencido nas linhas horizontais será verificada as colunas */
		if (coluna_1 == VITORIA_X || coluna_2 == VITORIA_X || coluna_3 == VITORIA_X) {
			vencedor = JOGADOR_X;
		}

		else if (coluna_1 == VITORIA_O || coluna_2 == VITORIA_O || coluna_3 == VITORIA_O) {
			vencedor = JOGADOR_O;
		}

		/* se ninguém tiver vecido nas colunas ou linhas, serão verificadas as diagonais */
		else {
			switch (tabuleiro[1][1]) {
				case JOGADOR_X:
					if ((tabuleiro[0][0] == JOGADOR_X && tabuleiro[2][2] == JOGADOR_X) ||
						(tabuleiro[0][2] == JOGADOR_X && tabuleiro[2][0] == JOGADOR_X)) {

						vencedor = JOGADOR_X;
					}
					break;

				case JOGADOR_O:
					if ((tabuleiro[0][0] == JOGADOR_O && tabuleiro[2][2] == JOGADOR_O) ||
						(tabuleiro[0][2] == JOGADOR_O && tabuleiro[2][0] == JOGADOR_O)) {

						vencedor = JOGADOR_O;
					}
			}
		}
	}


	if (vencedor == INDEFINIDO && pode_empatar) {
		printf("empate");
	}

	else if (vencedor == INDEFINIDO) {
		printf("em jogo");
	}

	else {
		printf("%c ganhou", vencedor);
	}

	return 0;
}

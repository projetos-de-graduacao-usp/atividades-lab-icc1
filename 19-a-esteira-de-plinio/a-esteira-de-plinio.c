#include <stdio.h>

#define DIREITA '>'
#define ESQUERDA '<'
#define CIMA '^'
#define BAIXO 'v'
#define JUNCAO '#'
#define INICIO '['
#define FIM ']'
#define BRANCO ' '
#define LOOP '.'
#define TAMANHO_FABRICA_X 65
#define TAMANHO_FABRICA_Y 32

int main()
{
	char mapa_da_fabrica[TAMANHO_FABRICA_Y][TAMANHO_FABRICA_X] = {};
	int coordenada_x;
	int coordenada_y;

	for (int i = 0; i < TAMANHO_FABRICA_Y; i++) {
		for (int j = 0; j < TAMANHO_FABRICA_X; j++) {
			scanf("%c", &mapa_da_fabrica[i][j]);
			if (mapa_da_fabrica[i][j] == INICIO) {
				coordenada_x = j + 2;
				coordenada_y = i;
			}
		}
	}

	char ultima_casa;

	do {
		char casa_atual = mapa_da_fabrica[coordenada_y][coordenada_x];

		if (casa_atual == DIREITA || casa_atual == ESQUERDA || casa_atual == CIMA ||
			casa_atual == BAIXO) {

			mapa_da_fabrica[coordenada_y][coordenada_x] = LOOP;
		}

		if (casa_atual == JUNCAO) {
			casa_atual = ultima_casa;
		}

		switch (casa_atual) {
			case DIREITA: coordenada_x += 2; break;
			case ESQUERDA: coordenada_x -= 2; break;
			case CIMA: coordenada_y -= 1; break;
			case BAIXO: coordenada_y += 1; break;
			case FIM: printf("Ok.\n"); break;
			case BRANCO: printf("Falha na esteira.\n"); break;
			case INICIO:
			case LOOP: printf("Loop infinito.\n");
		}

		ultima_casa = casa_atual;

	} while (ultima_casa != FIM && ultima_casa != LOOP && ultima_casa != BRANCO);


	for (int i = 0; i < TAMANHO_FABRICA_Y; i++) {
		for (int j = 0; j < TAMANHO_FABRICA_X; j++) {
			printf("%c", mapa_da_fabrica[i][j]);
		}
	}

	return 0;
}

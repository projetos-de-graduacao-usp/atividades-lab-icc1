#include <stdio.h>
#include <string.h>

#define TAMANHO_ROTOR 26

int main()
{
	scanf("%*[^\r\n]s"); // Ignora tudo até o final da linha

	int rotor_1[TAMANHO_ROTOR];
	for (int i = 0; i < TAMANHO_ROTOR; i++) {
		scanf("%d", &rotor_1[i]);
	}

	int rotor_2[TAMANHO_ROTOR];
	for (int i = 0; i < TAMANHO_ROTOR; i++) {
		scanf("%d", &rotor_2[i]);
	}

	int rotor_3[TAMANHO_ROTOR];
	for (int i = 0; i < TAMANHO_ROTOR; i++) {
		scanf("%d", &rotor_3[i]);
	}

	scanf("%*[\r\n]s");  // Ignora o pula linha
	scanf("%*[^\r\n]s"); // Ignora tudo até o final da linha

	char msg;
	int contador_rotor_1 = 0;
	int contador_rotor_2 = 0;
	int skip = 1;
	int stop;

	do {
		stop = (scanf("%c", &msg)) == EOF;

		// stop é a condição de parada, caso seja lido EOF a condição abaixo fará essa iteração
		// ser ignorada e assim nada será printado na tela.
		if (skip == 1 || stop == 1) {
			// Eu percebi que acontece uma coisa estranha onde meu código sempre lê um caractere
			// invisivel chamado LF (número 10 na tabela ascii) na primeira iteração,
			// isso faz o meu código não funcionar por isso também estou ignorando a primeira
			// iteração
			skip = 0;
			continue;
		}
		
		int char_index = 0;
		int is_upper = 0;
		int is_alphabetic = 0;

		// verificando valor do indice que o caractere de msg representa com base na tabela ascii
		if ((int)msg >= 65 && (int)msg <= 90) {
			char_index = (int)msg - 65;
			is_upper = 1;
			is_alphabetic = 1;
		}
		else if ((int)msg >= 97 && (int)msg <= 122) {
			char_index = (int)msg - 97;
			is_upper = 0;
			is_alphabetic = 1;
		}
		else {
			is_alphabetic = 0;
		}

		int caractere_decripitado = rotor_3[rotor_2[rotor_1[char_index]]];
		// printf("[%d]", caractere_decripitado);

		if (!is_alphabetic) {
			// se não for um caractere do alfabeto ele será printado na forma original
			printf("%c", msg);
			continue;
		}
		else if (is_upper) {
			printf("%c", caractere_decripitado + 65);
		}
		else {
			printf("%c", caractere_decripitado + 97);
		}

		contador_rotor_1 += 1;

		// faz a rotação do rotor 1
		for (int i = TAMANHO_ROTOR - 1; i >= 0; i--) {
			int valor_indice_atual;
			// inicializa o valor do indice do rotor atual como sendo o valor do primeiro
			// indice lido
			if (i == TAMANHO_ROTOR - 1)
				valor_indice_atual = rotor_1[i];

			// no caso de estarmos no indice zero o valor a ser modificado será o indice 25
			if (i == 0) {
				rotor_1[TAMANHO_ROTOR - 1] = valor_indice_atual;
				break;
			}

			// Salva o valor do indice anterior para ser usado na próxima iteração
			int valor_indice_anterior = rotor_1[i - 1];
			rotor_1[i - 1] = valor_indice_atual;
			// atribui ao valor do indice atual o valor do indice anteriro, que será o indice
			// da próxima iteração
			valor_indice_atual = valor_indice_anterior;
		}


		if (contador_rotor_1 == 26) {
			contador_rotor_2 += 1;
			contador_rotor_1 = 0;
		}
		else {
			continue;
		}
		// faz a rotação do rotor 2
		for (int i = TAMANHO_ROTOR - 1; i >= 0; i--) {
			int valor_indice_atual;

			if (i == TAMANHO_ROTOR - 1)
				valor_indice_atual = rotor_2[i];

			if (i == 0) {
				rotor_2[TAMANHO_ROTOR - 1] = valor_indice_atual;
				continue;
			}

			int valor_indice_anterior = rotor_2[i - 1];
			rotor_2[i - 1] = valor_indice_atual;

			valor_indice_atual = valor_indice_anterior;
		}


		if (contador_rotor_2 == 26) {
			contador_rotor_2 = 0;
		}
		else {
			continue;
		}
		// faz a rotação do rotor 3
		for (int i = TAMANHO_ROTOR - 1; i >= 0; i--) {
			int valor_indice_atual;

			if (i == TAMANHO_ROTOR - 1)
				valor_indice_atual = rotor_3[i];

			if (i == 0) {
				rotor_3[TAMANHO_ROTOR - 1] = valor_indice_atual;
				continue;
			}

			int valor_indice_anterior = rotor_3[i - 1];
			rotor_3[i - 1] = valor_indice_atual;

			valor_indice_atual = valor_indice_anterior;
		}

	} while (!stop);

	/* Meu deus, esse código ficou gigante. Em minha defesa eu não sei se o professor já tinha
	   passado sobre funções quando esse exercício foi proposto, por isso não usei elas */

	return 0;
}

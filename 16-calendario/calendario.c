#include <stdio.h>

int main()
{
	int dia_selecionado;
	scanf("%d", &dia_selecionado);

	printf("         Abril 2021         \n");
	printf(" Do  Se  Te  Qu  Qu  Se  Sa \n");

	int contador_dias = 1;
	// desloca pelas linhas
	for (int i = 0; i < 5; i++) {
		// desloca pelas colunas
		for (int j = 0; j < 7; j++) {
			// faz com que o mês começe na quinta
			if (i == 0 && j < 4) {
				printf("    ");
			}
			// mês termina no dia 30
			else if (i == 4 && j == 6) {
				break;
			}
			else {
				if (contador_dias != dia_selecionado) {
					printf(" %2d ", contador_dias);
				}
				else {
					printf("(%2d)", contador_dias);
				}
				contador_dias++;
			}
		}
		printf("\n");
	}
	return 0;
}

#include <stdio.h>

int main()
{
	int max_rodadas, num_jogadores, posicao;
	scanf("%d%d%d", &max_rodadas, &num_jogadores, &posicao);

	int repeticao_fragmento = 1, fragmento = 1, contador_de_repeticao = 0;
	int pinguins = 1, jogador = 1;

	for (int i = 1; i <= max_rodadas; i++) {

		if (jogador == posicao) {

			switch (fragmento) {
				case 1:
					i > 1 ? printf("%d pinguins\n", pinguins) : printf("%d pinguim\n", pinguins);
					break;
				case 2: printf("no gelo\n"); break;
				case 3: printf("uhuu!\n");
			}
		}

		contador_de_repeticao++;
		jogador == num_jogadores ? jogador = 1 : jogador++;

		// verifica de se o fragmento já foi repetido vezes o suficiente
		if (contador_de_repeticao == repeticao_fragmento) {

			if (fragmento == 3) {
				repeticao_fragmento++;
			}

			contador_de_repeticao = 0;
			fragmento == 3 ? fragmento = 1, pinguins++ : fragmento++;
		}
	}
	return 0;
}

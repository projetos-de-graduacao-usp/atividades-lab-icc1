/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IDENTIFICADOR 1
#define CURSO 2
#define TUDO 3

typedef struct registro {
	char *nome;
	char *curso;
	int numero_usp;
	int idade;
} Registro;

char *read_line();
void imprimir_registro_curso(Registro *lista);
int imprimir_registro_aluno(Registro *lista);
void imprimir_registro_tudo(Registro *lista);


int main()
{
	Registro *lista_registros = NULL;
	int cont_registros = 0;
	int topo_lista = 0;

	while (1) {
		topo_lista = cont_registros;
		cont_registros++;
		lista_registros = realloc(lista_registros, cont_registros * sizeof(Registro));

		scanf(" %d", &lista_registros[topo_lista].numero_usp);
		scanf(" %*[\n]");

		if (lista_registros[topo_lista].numero_usp == -1) {
			break;
		}

		lista_registros[topo_lista].nome = read_line();
		lista_registros[topo_lista].curso = read_line();
		scanf(" %d", &lista_registros[topo_lista].idade);
		scanf(" %*[\n]");
	}

	int comando = 0;

	do {
		scanf(" %i", &comando);
		scanf(" %*[\n]");

		switch (comando) {
			case IDENTIFICADOR:
				if (imprimir_registro_aluno(lista_registros) == 1) {
					printf("Aluno nao cadastrado\n");
				}
				break;

			case CURSO: imprimir_registro_curso(lista_registros); break;
			case TUDO: imprimir_registro_tudo(lista_registros); break;
		}

	} while (comando != -1);


	for (int i = 0; i < topo_lista; i++) {
		free(lista_registros[i].nome);
		free(lista_registros[i].curso);
	}

	free(lista_registros);

	return 0;
}


int imprimir_registro_aluno(Registro *lista)
{
	int registro;
	scanf(" %i", &registro);
	scanf(" %*[\n]");

	int indice = 0;

	while (1) {

		if (lista[indice].numero_usp == registro) {
			printf("Nome: %s\n", lista[indice].nome);
			printf("Curso: %s\n", lista[indice].curso);
			printf("N USP: %d\n", lista[indice].numero_usp);
			printf("IDADE: %d\n\n", lista[indice].idade);

			return 0;
		}

		indice++;
	}

	return 1;
}


void imprimir_registro_curso(Registro *lista)
{
	char *nome_curso = read_line();

	int indice = 0;

	do {

		if (strcmp(lista[indice].curso, nome_curso) == 0) {
			printf("Nome: %s\n", lista[indice].nome);
			printf("Curso: %s\n", lista[indice].curso);
			printf("N USP: %d\n", lista[indice].numero_usp);
			printf("IDADE: %d\n\n", lista[indice].idade);
		}

		indice++;

	} while (lista[indice].numero_usp != -1);

	free(nome_curso);
}


void imprimir_registro_tudo(Registro *lista)
{
	int indice = 0;

	do {
		printf("Nome: %s\n", lista[indice].nome);
		printf("Curso: %s\n", lista[indice].curso);
		printf("N USP: %d\n", lista[indice].numero_usp);
		printf("IDADE: %d\n\n", lista[indice].idade);

		indice++;
	} while (lista[indice].numero_usp != -1);
}


char *read_line()
{
	int contador_caracteres = 0;
	int indice;
	char *linha_lida = NULL;

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));

		scanf("%c", &linha_lida[indice]);

	} while (linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	linha_lida[indice] = '\0';

	scanf(" %*[\n]");

	return linha_lida;
}

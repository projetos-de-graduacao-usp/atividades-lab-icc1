#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define MAX_TAMANHO 76

int main()
{
	char linha[MAX_TAMANHO + 2];
	char caractere;
	int stop = FALSE;
	int spam = FALSE;
	int cont_blacklisted_words = 0;

	do {
		scanf("%[^\n]s", linha);

		stop = scanf("%c", &caractere) == EOF;

		if (strlen(linha) > MAX_TAMANHO) {
			spam = TRUE;
			break;
		}

		char palavra[50];
		int cont_caractere = 0;

		for (int i = 0; i < (int)strlen(linha); i++) {

			if (linha[i] != ' ' && linha[i] != '\t' && linha[i] != '\n' && linha[i] != '\r' &&
				linha[i] != ',' && linha[i] != '.' && linha[i] != '?' && linha[i] != '!') {

				palavra[cont_caractere] = linha[i];
				cont_caractere += 1;
				continue;
			}

			palavra[cont_caractere] = '\0';
			cont_caractere = 0;

			if (strcmp(palavra, "gratuito") == 0 || strcmp(palavra, "atencao") == 0 ||
				strcmp(palavra, "urgente") == 0 || strcmp(palavra, "imediato") == 0 ||
				strcmp(palavra, "zoombie") == 0 || strcmp(palavra, "oferta") == 0 ||
				strcmp(palavra, "dinheiro") == 0 || strcmp(palavra, "renda") == 0 ||
				strcmp(palavra, "fundo") == 0 || strcmp(palavra, "limitado") == 0 ||
				strcmp(palavra, "ajuda") == 0 || strcmp(palavra, "SOS") == 0) {

				cont_blacklisted_words += 1;
			}
		}

		if (cont_blacklisted_words >= 2) {
			spam = TRUE;
			break;
		}
		
	} while (!stop);


	if (spam) {
		printf("Spam\n");
	}
	else {
		printf("Inbox\n");
	}

	return 0;
}
